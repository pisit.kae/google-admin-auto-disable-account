
import pandas as pd
import pyautogui
import time
import os
from PIL import Image
import sys
from datetime import datetime
sys.path.append("Services/src")
from FindImg import *

dataFile = pd.read_csv('input/email.csv')
dt_stringDir = datetime.now().strftime("%d-%m-%Y%H%M%S")
outputFolder = 'output/' + dt_stringDir
result = []
os.mkdir(outputFolder)
os.mkdir(outputFolder+'/screenshot')

#def saveResult(dataToSave, outputFolder) : 
#    resultDF = pd.DataFrame(dataToSave)
#    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
#    dt_stringDir = datetime.now().strftime("%d-%m-%Y%H%M%S")
#    print('finished at ',dt_string)
#    resultDF.to_csv(outputFolder+'/'+dt_stringDir+'.csv', index=False,header=None)

try:
    countDown()
    # print(dataFile.loc[0].automateStatus)

    for index, row in dataFile.iterrows():
        print('Processing [', index, '] => ', row['Email'])
        findImgAndClick('input/search.png', 100)
        pyautogui.hotkey('ctrl', 'a')
        pyautogui.write(row['Email'])
        time.sleep(0.5)
        pyautogui.hotkey('enter')
        findImgOnScreen('input/searchloaded.png')
        found = findImg('input/found.png')
        if(found is None):
            dataFile.at[index, 'automateStatus'] = 'not found'
            pyautogui.screenshot(outputFolder+'/screenshot/'+row['Email']+'.png')
            print(('save screenshot at '+outputFolder+'/screenshot/'+row['Email']+'.png'))
            pyautogui.moveTo(800, 200)
            pyautogui.click()
            continue

        findImgAndClick('input/found.png', 10, 150)
        findImgOnScreen('input/loaded.png')
        foundSus = findImg('input/sus.png')
        if(foundSus is None):
            foundRe = findImg('input/reactivate.png')
            if(foundRe is None):
                dataFile.at[index, 'automateStatus'] = 'Error'
            else : 
                dataFile.at[index, 'automateStatus'] = 'Suspended'
        else : 
            dataFile.at[index, 'automateStatus'] = 'Not Suspended'

        pyautogui.screenshot(outputFolder+'/screenshot/'+row['Email']+'.png')
        print(('save screenshot at '+outputFolder+'/screenshot/'+row['Email']+'.png'))
        pyautogui.moveTo(800, 200)
        pyautogui.click()
    saveResult(dataFile, outputFolder)
except Exception as e:
    print(e)
    saveResult(dataFile, outputFolder)
    input('Enter any key to close...')
finally:
    input('Enter any key to close...')