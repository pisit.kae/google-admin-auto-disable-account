import pandas as pd
import pyautogui
import time
import os
from PIL import Image
import sys
from datetime import datetime

def countDown(sec = 5):
    for x in range(sec):
        time.sleep(1)
        print('waiting ' + str(x+1))

def findImgAndClick(_path, _x=10, _y=10):
    Img = pyautogui.locateOnScreen(_path, confidence=0.9)
    while Img is None:
        print('cannot find ' + _path)
        time.sleep(1)
        Img = pyautogui.locateOnScreen(_path, confidence=0.9)
    print('found ' + _path)
    pyautogui.moveTo(Img[0]+_x, Img[1]+_y)
    time.sleep(0.5)
    pyautogui.click()
    time.sleep(0.5)
    return Img


def findImgOnScreen(_path):
    Img = pyautogui.locateOnScreen(_path, confidence=0.9)
    while Img is None:
        print('cannot find ' + _path + ' now waiting...')
        time.sleep(1)
        Img = pyautogui.locateOnScreen(_path, confidence=0.9)
    print('found ' + _path)
    return Img

def findImg(_path):
    Img = pyautogui.locateOnScreen(_path, confidence=0.9)
    return Img

def untilNotFound(_path):
    Img = pyautogui.locateOnScreen(_path, confidence=0.9)
    while Img is not None:
        print('found ' + _path + ' now waiting...')
        time.sleep(1)
        Img = pyautogui.locateOnScreen(_path, confidence=0.9)
    print('can not find ' + _path + 'anymore')
    return true

def saveResult(dataToSave, outputFolder) : 
    resultDF = pd.DataFrame(dataToSave)
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    dt_stringDir = datetime.now().strftime("%d-%m-%Y%H%M%S")
    print('finished at ',dt_string)
    resultDF.to_csv(outputFolder+'/'+dt_stringDir+'.csv', index=False,header=None)